import { StatusBar } from 'expo-status-bar';
import React,{useState, useEffect} from 'react';
import { Alert,StyleSheet, Text, View } from 'react-native';

import Click from './src/Click';

export default function App() {
  const [count, setCount] = useState(10);

  let countString = "count in App:"+count;

  function updateCount(newCount){

    setCount(newCount);

  }
  useEffect(()=>{
  Alert.alert("count in App:"+count);});
  
  return (
    <View style={styles.container}>
      <Text>Hello Word!!</Text>
      <Click count={count} update={updateCount}/>
      
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
